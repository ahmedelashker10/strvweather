//
//  Utility.swift
//  STRVWeather
//
//  Created by Ahmed Elashker on 7/2/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

import UIKit

class Utility: NSObject {
    
    static var apiManager = APIManager()
    static var loadingVC = LoadingViewController(nibName: "LoadingViewController", bundle: nil)
    
    // MARK: Preparing UI Methods
    class func prepareUIForLoadingState(inViewController viewController:UIViewController, withMessage message:String) -> Void {
        let selectedIndex = (viewController.parent as! TabViewController).selectedIndex
        if ((viewController.parent as! TabViewController).childViewControllers[selectedIndex] == viewController) {
            loadingVC.view.isHidden = false
            
            loadingVC.activityIndicator.isHidden = false
            loadingVC.activityIndicator.startAnimating()
            
            Utility.hideSubviewsInView(hidden: true, view: viewController.view)
            Utility.enableBarItems(enabled: false, inTabViewController: (viewController.parent as! TabViewController))
            
            Utility.removeLoadingView()
            Utility.addLoadingViewToView(view: viewController.view)
            Utility.updateLoadingViewMessage(message: message)
        }
    }
    
    class func prepareUIForErrorState(inViewController viewController:UIViewController, withMessage message:String) -> Void {
        let selectedIndex = (viewController.parent as! TabViewController).selectedIndex
        if ((viewController.parent as! TabViewController).childViewControllers[selectedIndex] == viewController) {
            loadingVC.view.isHidden = false
            
            loadingVC.activityIndicator.isHidden = true
            loadingVC.activityIndicator.stopAnimating()
            
            Utility.addLoadingViewToView(view: viewController.view)
            Utility.enableBarItems(enabled: false, inTabViewController: (viewController.parent as! TabViewController))
            Utility.updateLoadingViewMessage(message: message)
        }
    }
    
    class func prepareUIForNormalState(inViewController viewController:UIViewController) -> Void {
        let selectedIndex = (viewController.parent as! TabViewController).selectedIndex
        if ((viewController.parent as! TabViewController).childViewControllers[selectedIndex] == viewController) {
            loadingVC.view.isHidden = true
            Utility.removeLoadingView()
            Utility.hideSubviewsInView(hidden: false, view: viewController.view)
            Utility.enableBarItems(enabled: true, inTabViewController: (viewController.parent as! TabViewController))
        }
    }
    
    // MARK: Loading View Methods
    class func addLoadingViewToView(view:UIView) -> Void {
        loadingVC.view.frame = UIScreen.main.bounds
        view.addSubview(loadingVC.view)
    }
    
    class func removeLoadingView() -> Void {
        loadingVC.view.removeFromSuperview()
    }
    
    class func isLoadingViewSubviewOf(view:UIView) -> Bool {
        return view.subviews.contains(loadingVC.view)
    }
    
    class func updateLoadingViewMessage(message:String) -> Void {
        loadingVC.lblLoading.text = message
    }
    
    // MARK: General UI Methods
    class func hideSubviewsInView(hidden:Bool, view:UIView) -> Void {
        for subview in view.subviews {
            subview.isHidden = hidden
        }
    }
    
    class func enableBarItems(enabled:Bool, inTabViewController tabController:UITabBarController) -> Void {
        for barItem in (tabController.tabBar.items)! {
            barItem.isEnabled = enabled
        }
    }
    
    // MARK: Date Methods
    class func dateFromString(dateString:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.date(from: dateString)!
    }
    
    class func hourStringFromDate(date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:ss"
        return dateFormatter.string(from: date)
    }
    
    class func dateWithDayFromDate(daylessDate:Date, dayDate:Date) -> Date {
        var dayDateComponents: DateComponents? = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: daylessDate)
        dayDateComponents?.day = Calendar.current.component(.day, from: dayDate)
        return Calendar.current.date(from: dayDateComponents!)!
    }
    
    // MARK: Weather Methods
    class func windDirectionFromDegrees(degrees:Float) -> String {
        let directions = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"]
        let i: Int = Int((degrees + 11.25)/22.5)
        return directions[i % 16]
    }
    
    class func imageNameForForecast(forecast:WeatherForecast, sunriseDate:Date, sunsetDate:Date, isMainLogo:Bool) -> String {
        let weatherID = forecast.weather![0].id
        var imageName = Utility.imageNameForWeatherID(weatherID: weatherID!, isMainLogo: isMainLogo)
        let forecastDate = Date(timeIntervalSince1970: forecast.dt!)
        let sunriseDateOnForecastDay = Utility.dateWithDayFromDate(daylessDate: sunriseDate, dayDate: forecastDate)
        let sunsetDateOnForecastDay = Utility.dateWithDayFromDate(daylessDate: sunsetDate, dayDate: forecastDate)
        
        if(sunriseDateOnForecastDay < forecastDate && sunsetDateOnForecastDay > forecastDate) {
            imageName += " (Day)"
        }
        else {
            imageName += " (Night)"
        }
        return imageName
    }
    
    class func imageNameForWeatherID(weatherID:Int, isMainLogo:Bool) -> String {
        var imageName = isMainLogo ? "100x100 " : "60x60 "
        
        switch weatherID {
        case 200, 201, 202, 210, 211, 212, 221, 230, 231, 232:
            imageName += "Thunderstorm"
        case 300, 301, 302, 310, 311, 312, 313, 314, 321:
            imageName += "Shower Rain"
        case 500, 501, 502, 503, 504, 511, 520, 521, 522, 531:
            imageName += "Rain"
        case 600, 601, 602, 611, 612, 615, 616, 620, 621, 622:
            imageName += "Snow"
        case 701, 711, 721, 731, 741, 751, 761, 762, 771, 781:
            imageName += "Mist"
        case 800:
            imageName += "Clear Sky"
        case 801, 802, 803, 804:
            imageName += "Scattered Clouds"
        default:
            imageName += "Clear Sky"
        }
        
        return imageName
    }
}
