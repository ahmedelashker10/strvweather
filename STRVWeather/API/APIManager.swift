//
//  APIManager.swift
//  STRVWeather
//
//  Created by Ahmed Elashker on 7/1/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper

class APIManager: NSObject {
    
    let apiKey = "48499d0032cea700228e3799bd98a113"
    let weatherURL = "http://api.openweathermap.org/data/2.5/weather"
    let fiveDaysForecastURL = "http://api.openweathermap.org/data/2.5/forecast"
    
    func getWeather(lat: Double, lon:Double, completion: @escaping (WeatherForecast?) -> Void) {
        Alamofire.request(weatherURL,
                          parameters: ["lat": lat, "lon":lon, "APPID":apiKey, "units":"metric"],
                          headers: ["": ""])
            .responseJSON { response in
                guard response.result.isSuccess,
                    let forecast = Mapper<WeatherForecast>().map(JSONObject:response.result.value) else {
                        print("Error while fetching results: \(String(describing: response.result.error))")
                        completion(nil)
                        return
                }

                completion(forecast)
        }
    }
    
    func get5DaysForecast(lat: Double, lon:Double, completion: @escaping (FiveDaysForecast?) -> Void) {
        Alamofire.request(fiveDaysForecastURL,
                          parameters: ["lat": lat, "lon":lon, "APPID":apiKey, "units":"metric"],
                          headers: ["": ""])
            .responseJSON { response in
                guard response.result.isSuccess,
                    let forecast = Mapper<FiveDaysForecast>().map(JSONObject:response.result.value) else {
                        print("Error while fetching results: \(String(describing: response.result.error))")
                        completion(nil)
                        return
                }
                
                completion(forecast)
       }
    }
}
