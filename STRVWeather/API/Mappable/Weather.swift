//
//  Weather.swift
//  STRVWeather
//
//  Created by Ahmed Elashker on 7/2/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

import UIKit
import ObjectMapper

class Weather: NSObject, Mappable {
    required init?(map: Map) {
        super.init()
    }
    
    func mapping(map: Map) {
        desc <- map["description"]
        icon <- map["icon"]
        id <- map["id"]
        main <- map["main"]
    }
    
    var desc:String!
    var icon:String!
    var id:Int!
    var main:String!
}
