//
//  Wind.swift
//  STRVWeather
//
//  Created by Ahmed Elashker on 7/2/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

import UIKit
import ObjectMapper

class Wind: NSObject, Mappable {
    required init?(map: Map) {
        super.init()
    }
    
    func mapping(map: Map) {
        deg <- map["deg"]
        speed <- map["speed"]
    }
    
    var deg:Float!
    var speed:Float!
}
