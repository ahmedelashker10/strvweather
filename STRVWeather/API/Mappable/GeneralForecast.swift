//
//  GeneralForecast.swift
//  STRVWeather
//
//  Created by Ahmed Elashker on 7/3/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

import UIKit
import ObjectMapper

class GeneralForecast: NSObject, Mappable {
    required init?(map: Map) {
        super.init()
    }
    
    func mapping(map: Map) {
        country <- map["country"]
        id <- map["id"]
        message <- map["message"]
        sunrise <- map["sunrise"]
        sunset <- map["sunset"]
        type <- map["type"]
        pod <- map["pod"]
    }
    
    var country:String!
    var id:Int!
    var message:Float!
    var sunrise:Double!
    var sunset:Double!
    var type:Int!
    var pod:String!
}
