//
//  FiveDaysForecast.swift
//  STRVWeather
//
//  Created by Ahmed Elashker on 7/2/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

import UIKit
import ObjectMapper

class FiveDaysForecast: NSObject, Mappable {
    required init?(map: Map) {
        super.init()
    }
    
    func mapping(map: Map) {
        city <- map["city"]
        cnt <- map["cnt"]
        cod <- map["cod"]
        list <- map["list"]
        message <- map["message"]
    }
    
    var city:Dictionary<String, Any>?
    var cnt:String?
    var cod:Int?
    var list:[WeatherForecast]?
    var message:Float?
}
