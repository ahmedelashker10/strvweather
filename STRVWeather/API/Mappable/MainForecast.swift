//
//  MainForecast.swift
//  STRVWeather
//
//  Created by Ahmed Elashker on 7/2/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

import UIKit
import ObjectMapper

class MainForecast: NSObject, Mappable {
    required init?(map: Map) {
        super.init()
    }
    
    func mapping(map: Map) {
        grnd_level <- map["grnd_level"]
        sea_level <- map["sea_level"]
        humidity <- map["humidity"]
        pressure <- map["pressure"]
        temp <- map["temp"]
        temp_kf <- map["temp_kf"]
        temp_max <- map["temp_max"]
        temp_min <- map["temp_min"]
    }
    
    var grnd_level:Float!
    var sea_level:Float!
    var humidity:Int!
    var pressure:Int!
    var temp:Int!
    var temp_kf:Int!
    var temp_max:Int!
    var temp_min:Int!
}
