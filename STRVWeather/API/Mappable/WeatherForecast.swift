//
//  WeatherForecast.swift
//  STRVWeather
//
//  Created by Ahmed Elashker on 7/2/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

import UIKit
import ObjectMapper

class WeatherForecast: NSObject, Mappable {
    required init?(map: Map) {
        super.init()
    }
    
    func mapping(map: Map) {
        base <- map["base"]
        clouds <- map["clouds"]
        cod <- map["cod"]
        coord <- map["coord"]
        dt <- map["dt"]
        dt_txt <- map["dt_txt"]
        id <- map["id"]
        main <- map["main"]
        name <- map["name"]
        rain <- map["rain"]
        sys <- map["sys"]
        visibility <- map["visibility"]
        weather <- map["weather"]
        wind <- map["wind"]
    }
    

    var base:String?
    var clouds:Dictionary<String, Any>?
    var cod:Int?
    var coord:Dictionary<String, Any>?
    var dt:Double?
    var dt_txt:String?
    var id:Int?
    var main:MainForecast?
    var name:String?
    var rain:Dictionary<String, Any>?
    var sys:GeneralForecast?
    var visibility:Int?
    var weather:[Weather]?
    var wind:Wind?
}
