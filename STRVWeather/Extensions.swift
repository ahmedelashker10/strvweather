//
//  Extensions.swift
//  STRVWeather
//
//  Created by Ahmed Elashker on 7/2/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

import Foundation

extension Int {
    var stringValue:String {
        return "\(self)"
    }
}

extension Float {
    var stringValue:String {
        return "\(self)"
    }
}
