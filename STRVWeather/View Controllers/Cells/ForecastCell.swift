//
//  ForecastCell.swift
//  STRVWeather
//
//  Created by Ahmed Elashker on 7/1/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

import UIKit

class ForecastCell: UITableViewCell {

    @IBOutlet var imgViewCondition: UIImageView!
    
    @IBOutlet var lblTime: UILabel!
    
    @IBOutlet var lblCondition: UILabel!
    
    @IBOutlet var lblTemprature: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
