//
//  TodayViewController.swift
//  STRVWeather
//
//  Created by Ahmed Elashker on 6/30/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftyJSON
import Reachability

class TodayViewController: UIViewController, CLLocationManagerDelegate {
    
    // MARK: IB Outlets
    @IBOutlet var imgViewCondition: UIImageView!
    
    @IBOutlet var lblLocation: UILabel!
    
    @IBOutlet var lblTemprature: UILabel!
    
    @IBOutlet var lblCondition: UILabel!
    
    @IBOutlet var lblHumidity: UILabel!
    
    @IBOutlet var lblPrecipitation: UILabel!
    
    @IBOutlet var lblPressure: UILabel!
    
    @IBOutlet var lblWind: UILabel!
    
    @IBOutlet var lblWindDirection: UILabel!
    
    @IBOutlet var loadingView: UIView!
    
    // MARK: Private Properties
    var locationManager = CLLocationManager()
    var geoCoder = CLGeocoder()
    var forecast:WeatherForecast?
    let reachability = Reachability(hostname: "www.google.com")!
    
    // MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupReachability()
        
        if(!Utility.isLoadingViewSubviewOf(view: view)) {
            Utility.prepareUIForLoadingState(inViewController: self, withMessage: "Getting Location")
            self.startLocationManager()
        }
    }
    
    func setupReachability() -> Void {
        reachability.whenReachable = { reachability in
            Utility.prepareUIForLoadingState(inViewController: self, withMessage: "Getting Location")
            self.startLocationManager()
        }
        reachability.whenUnreachable = { reachability in
            Utility.prepareUIForErrorState(inViewController: self, withMessage: "An error occured. Please check your connection and try again.")
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Location Manager Setup
    func startLocationManager() -> Void {
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    // MARK: Location Manager Update
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        
        guard let location: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        (parent as! TabViewController).location = location
        
        fillAddressWithLocation(location: manager.location!)
        fetchWeatherWithLocationCoordinate(location: location)
    }
    
    // MARK: Location Lbl Set Value
    func fillAddressWithLocation(location:CLLocation) -> Void {
        geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
            if(placemarks == nil) {
                return
            }
            
            let currentPlacemark = placemarks![0]
            let city = currentPlacemark.locality
            let country = currentPlacemark.country
            self.lblLocation.text = city! + ", " + country!
            
            (self.parent as! TabViewController).city = city
        }
    }
    
    // MARK: API Call Weather For Location
    func fetchWeatherWithLocationCoordinate(location:CLLocationCoordinate2D) -> Void {
        Utility.updateLoadingViewMessage(message: "Getting Weather")
        Utility.apiManager.getWeather(lat: Double(location.latitude), lon: Double(location.longitude),  completion: { (forecast) in
            if(forecast == nil) {
                Utility.prepareUIForErrorState(inViewController: self, withMessage: "An error occured. Please check your connection and try again.")
            }
            else {
                Utility.prepareUIForNormalState(inViewController: self)
                
                if(forecast == nil) {
                    return
                }
                
                self.fillPageFromWeatherForecast(forecast: forecast)
                
                self.forecast = forecast
                (self.parent as! TabViewController).forecast = forecast
            }
        })
    }
    
    // MARK: API Response Weather Forecast
    func fillPageFromWeatherForecast(forecast:WeatherForecast?) -> Void {
        if(forecast == nil) {
            return
        }
        
        fillMainForecast(forecast: forecast!)
        checkRain(forecast: forecast!)
        
        imgViewCondition.image = UIImage(named: imageNameForForecast(forecast: forecast!))
    }
    
    func imageNameForForecast(forecast:WeatherForecast) -> String {
        let sunriseDate = Date(timeIntervalSince1970: (forecast.sys?.sunrise)!)
        let sunsetDate = Date(timeIntervalSince1970: (forecast.sys?.sunset)!)
        
        return Utility.imageNameForForecast(forecast: forecast,
                                            sunriseDate: sunriseDate,
                                            sunsetDate: sunsetDate,
                                            isMainLogo: true)
    }
    
    func fillMainForecast(forecast:WeatherForecast) -> Void {
        lblHumidity.text = (forecast.main?.humidity.stringValue)! + "%"
        lblPressure.text = (forecast.main?.pressure.stringValue)! + " hPa"
        lblTemprature.text = (forecast.main?.temp.stringValue)! + "ºC"
        lblCondition.text = forecast.weather![0].main
        lblWind.text = (forecast.wind?.speed.stringValue)! + "km/h"
        lblWindDirection.text = Utility.windDirectionFromDegrees(degrees: (forecast.wind?.deg)!)
    }
    
    func checkRain(forecast:WeatherForecast) -> Void {
        if let rain = forecast.rain {
            lblPrecipitation.text =  String(describing: rain["3h"]) + " mm"
        }
        else {
            lblPrecipitation.text = "0 mm"
        }
    }
    
    // MARK: Location Manager Fail
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        Utility.removeLoadingView()
        
        if(Utility.isLoadingViewSubviewOf(view: view)) {
            return
        }
        
        if(CLLocationManager.authorizationStatus() == .denied) {
            Utility.prepareUIForErrorState(inViewController: self, withMessage: "Give permission for the app to obtain your location from privacy settings")
        }
    }
    
    // MARK: Action Handlers
    @IBAction func btnShare_Clicked(_ sender: Any) {
        let textToShare = [getShareableForecast()]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func getShareableForecast() -> String {
        var forecastText = "Current Weather Forecast \n Humidity: "
        forecastText += (forecast?.main?.humidity.stringValue)!
        forecastText += "% \n Temprature: "
        forecastText += (forecast?.main?.temp.stringValue)!
        forecastText += "ºC \n Pressure: "
        forecastText += (forecast?.main?.pressure.stringValue)!
        forecastText += " hPa"
        return forecastText
    }
}
