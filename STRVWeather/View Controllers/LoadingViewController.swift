//
//  LoadingViewController.swift
//  STRVWeather
//
//  Created by Ahmed Elashker on 7/3/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {

    // MARK: IB Outlets
    @IBOutlet var lblLoading: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    // MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
