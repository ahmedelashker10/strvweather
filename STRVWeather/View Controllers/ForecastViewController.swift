//
//  ForecastViewController.swift
//  STRVWeather
//
//  Created by Ahmed Elashker on 7/1/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

import UIKit
import CoreLocation
import Reachability

class ForecastViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // MARK: IB Outlets
    @IBOutlet var tableView: UITableView!
    @IBOutlet var lblHeader: UILabel!
    
    // MARK: Properties
    var location:CLLocationCoordinate2D?
    var weekForecast:FiveDaysForecast?
    var mainForecast:WeatherForecast?
    let reachability = Reachability(hostname: "www.google.com")!
    
    // MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        Utility.hideSubviewsInView(hidden: true, view: view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getSharedPropertiesFromParent()
        setupReachability()
        
        if(!Utility.isLoadingViewSubviewOf(view: view)) {
            Utility.prepareUIForLoadingState(inViewController: self, withMessage: "Getting Forecast")
            self.fetchForecastWithLocationCoordinate(location: self.location!)
        }
    }
    
    func getSharedPropertiesFromParent() -> Void {
        mainForecast = (parent as! TabViewController).forecast
        location = (parent as! TabViewController).location
        lblHeader.text = (parent as! TabViewController).city
    }
    
    func setupReachability() -> Void {
        reachability.whenReachable = { reachability in
            Utility.prepareUIForLoadingState(inViewController: self, withMessage: "Getting Forecast")
            self.fetchForecastWithLocationCoordinate(location: self.location!)
        }
        reachability.whenUnreachable = { reachability in
            Utility.prepareUIForErrorState(inViewController: self, withMessage: "An error occured. Please check your connection and try again.")
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: API Call 5 Days Forecast
    func fetchForecastWithLocationCoordinate(location:CLLocationCoordinate2D) -> Void {
        clearTable()
        Utility.apiManager.get5DaysForecast(lat: Double(location.latitude), lon: Double(location.longitude)) { (forecast) in
            if(forecast == nil) {
                Utility.prepareUIForErrorState(inViewController: self, withMessage: "An error occured. Please check your connection and try again.")
            }
            else {
                Utility.prepareUIForNormalState(inViewController: self)
                
                if(forecast == nil) {
                    return
                }
                
                self.weekForecast = forecast
                self.tableView.reloadData()
            }
        }
    }
    
    func clearTable() -> Void {
        if(weekForecast != nil && weekForecast?.list != nil) {
            weekForecast?.list = []
            tableView.reloadData()
        }
    }
    
    // MARK: Table View Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterWeatherByDay(day: section).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastCell", for: indexPath) as! ForecastCell
        
        let dayForecast = filterWeatherByDay(day: indexPath.section)
        let forecast = dayForecast[indexPath.row]
        cell.imgViewCondition.image = UIImage(named: imageNameForForecast(forecast: forecast))
        cell.lblTemprature.text = (forecast.main?.temp.stringValue)! + "ºC"
        cell.lblCondition.text = forecast.weather![0].main
        
        let date = Utility.dateFromString(dateString: forecast.dt_txt!)
        cell.lblTime.text = Utility.hourStringFromDate(date: date)
        
        return cell
    }
    
    func filterWeatherByDay(day:Int) -> [WeatherForecast] {
        let dateForDay = Calendar.current.date(byAdding: .day, value: day, to: Date())
        let results:NSMutableArray = []
        if(weekForecast != nil && weekForecast?.list != nil) {
            for weatherForecast in (weekForecast?.list)! {
                let dayDate = Utility.dateFromString(dateString: weatherForecast.dt_txt!)
                if Calendar.current.compare(dayDate, to: dateForDay!, toGranularity: .day) == .orderedSame {
                    results.add(weatherForecast)
                }
            }
        }
        
        return Array(results) as! [WeatherForecast]
    }
    
    func imageNameForForecast(forecast:WeatherForecast) -> String {
        let sunriseDate = Date(timeIntervalSince1970: (mainForecast?.sys?.sunrise)!)
        let sunsetDate = Date(timeIntervalSince1970: (mainForecast?.sys?.sunset)!)
        
        return Utility.imageNameForForecast(forecast: forecast,
                                            sunriseDate: sunriseDate,
                                            sunsetDate: sunsetDate,
                                            isMainLogo: false)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 60))
        view.backgroundColor = .white
        
        let label = UILabel(frame: CGRect(x: 20, y: 20, width: tableView.frame.width, height: 14))
        label.font = UIFont(name: "ProximaNova-Semibold", size: 15)
        if(section == 0) {
            label.text = "Today"
        }
        else {
            let upcomingDate = Calendar.current.date(byAdding: .day, value: section, to: Date())
            let formatter = DateFormatter()
            formatter.dateFormat = "EEEE"
            label.text = formatter.string(from: upcomingDate!)
        }
        view.addSubview(label)
        
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x: 0.0, y:43.0, width: tableView.frame.width, height: 1)
        bottomBorder.backgroundColor = UIColor.lightGray.cgColor
        view.layer.addSublayer(bottomBorder)
        return view
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.backgroundColor = .white
    }
}
