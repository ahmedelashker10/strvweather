//
//  TabViewController.swift
//  STRVWeather
//
//  Created by Ahmed Elashker on 6/30/18.
//  Copyright © 2018 Ahmed Elashker. All rights reserved.
//

import UIKit
import CoreLocation

class TabViewController: UITabBarController {

    // MARK: Shared properties
    var location: CLLocationCoordinate2D?
    var city: String?
    var forecast:WeatherForecast?
    
    // MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
